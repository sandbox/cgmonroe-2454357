
CloudFront Refresh Module
=========================


Description
-----------
This module allows sites using CloudFront as a CDN to send 'Invalidate' 
requests for nodes that have been updated when the site cache is cleared. This
helps get around CloudFront's 3 active Invalidation request limit and to cut
down on the total file invalidations (CF charges for > 3000).  E.g., a node can
be edited multiple times but only invalidated once when the cache is cleared.

It also supplies a form that can be used to manually specify URLs to be 
invalidated (e.g. a PDF file's content was changed but name didn't, and 
the like).

Optionally, a set of paths to always be refreshed can be defined. For example, 
if your front page shows node teasers and these need to be updated when a 
node's text changes, you could add <front> to this list.

This module supplies a simple "status" page that shows the last 20 
invadation requests made to CloudFront.  Recent requests will show up as 
'in process' until Amazon has finished them.

It also has several methods for making sure the CDN is being used.  These are:

- Modifying the script and CSS URLs to not include a domain.  This can be a 
  problem is the page is cached while an editor is using the origin URL. The 
  origin URL will be stored in the cached version and added to the CDN.  Now
  all the Javascript and CSS are not being served by the CDN.
  
- If the Advanced Agregation (advagg) module is used and the cdn host name is
  set in the configuration, the aggregation "context" will be forced to use
  the CDN hostname rather than the current host name.  This helps ensure that
  aggregated CSS will pull from the CDN and not the origin server.
  
- EXPERIMENTAL TEST FIRST! There is experimental support for using the expires
  module instead of just node update to trigger invalidate requests.  This has
  not been fully tested, so use at your own risk.

Installation
------------
1) Install the libraries (v2.0 or greater) module if needed.

2) Install the AWSSDK module ( http://drupal.org/project/awssdk )

3) Install the AWSSDK php library (see: https://www.drupal.org/node/1396596 )

4) Install the module as you normally would. 

5) Enable the following modules:  libraries (if not enabled), awssdk, awssdk_ui,
   and cloudfront_refresh.

6) You will need the following information from your Amazon console:

  a) The CDN distribution id
  
  b) An application user's web services key and the secret key.
  
7) Configure the AWS SDK module with the information from 6b) by going to:

   admin/config/media/awssdk
   
8) Configure the CloudFront refresh setting with the info from 6a) by going to:

   admin/config/development/cloudfront_refresh/settings  

Optional Settings:

  TBD - See field comments for now


Permission
----------

The module has 2 permisions that need to be set on the appropriate roles.  These
are:

  Administer CloudFront CDN:
  
    Users with this can view / configure the module settings.

  Purge CloudFront:
  
    User with this can manually specify paths to clear from the CDN cache.
