<?php
/**
 * @file
 * The cache handler object for the 'cloudfront_refresh' bin.
 */

/**
 * Pseudo cache handler used to process refresh queue when cache is cleared.
 */
class CloudFrontRefreshCache extends DrupalFakeCache implements DrupalCacheInterface {

  /**
   * Overrides DrupalDatabaseCache::clear().
   */
  public function clear($cid = NULL, $wildcard = FALSE) {
    cloudfront_refresh_process_queue();
  }

}
