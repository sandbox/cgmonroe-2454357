<?php
/**
 * @file
 * The hook_node_* methods.
 */

/**
 * Clear the various URLs related to a node.
 *
 * @param stdClass $node
 *   The node object being refreshed.
 */
function cloudfront_refresh_node(stdClass $node) {

  $paths = array();
  $paths[] = 'node/' . $node->nid;
  cloudfront_refresh_queue_invalidations($paths);
}
/**
 * Implements hook_node_insert().
 */
function cloudfront_refresh_node_insert($node) {

  // Only invalidate the 'always purge' links for new node.
  $paths = cloudfront_refresh_always_purge_paths();
  if (!empty($paths)) {
    cloudfront_refresh_queue_invalidations($paths);
  }
}
/**
 * Implements hook_node_update().
 */
function cloudfront_refresh_node_update($node) {
  cloudfront_refresh_node($node);
}
/**
 * Implements hook_node_delete().
 */
function cloudfront_refresh_node_delete($node) {
  cloudfront_refresh_node($node);
}
