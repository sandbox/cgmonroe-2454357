<?php
/**
 * @file
 * Page code for the cloudfront_refresh module.
 */

/**
 * The settings form.
 *
 * @param array $form
 *   The Form array.
 * @param array $form_state
 *   The Form state array.
 *
 * @return array
 *   The settings form array.
 */
function cloudfront_refresh_settings_form(array $form, array &$form_state) {

  $form = array();
  if (!variable_get('aws_key', '') || !variable_get('aws_secret', '')) {
    drupal_set_message(t('The AWSSDK module security information has not been set yet. This is required for this module to work.'), 'warning');
  }

  $form['cloudfront_refresh_distribution_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Amazon CloudFront Distribution ID'),
    '#description' => t("Enter the CloudFront Distribution ID used as this site's CDN."),
    '#default_value' => variable_get('cloudfront_refresh_distribution_id'),
    '#required' => TRUE,
  );

  $form['cloudfront_refresh_purge_always'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths to Alway Refresh'),
    '#rows' => 6,
    '#description' => t('Paths entered here will be refreshed anytime an request is sent to the CDN (e.g. cache cleared). Enter one path per line. Paths will be normalized to root paths, e.g. /node/1234. Paths that do not start with a / will be checked for aliases. The home page can be identified by &lt;front&gt;. Wild cards are NOT supported.'),
    '#default_value' => variable_get('cloudfront_refresh_purge_always'),
  );

  $form['cloudfront_refresh_host_name'] = array(
    '#type' => 'textfield',
    '#title' => t('The DNS host name for the CloudFront'),
    '#description' => t('Optionally, enter the dns name, e.g. www.example.com, of the CF server.  This will be used by the Advanced Aggregation module to produce aggregated CSS files that use the CDN server.'),
    '#default_value' => variable_get('cloudfront_refresh_host_name'),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}
/**
 * Form to refresh content by manually entered URLs.
 *
 * @return array
 *   Form API array.
 */
function cloudfront_refresh_selective_purge_form() {
  $form = array();
  if (!variable_get('aws_key', '') || !variable_get('aws_secret', '')) {
    drupal_set_message(t('The AWSSDK module security information has not been set yet. This is required for this module to work.'), 'warning');
  }
  if (!variable_get('cloudfront_refresh_distribution_id', '')) {
    drupal_set_message(t('Cloudfront Invalidate distribution id has not be set.'), 'warning', FALSE);
  }

  $form['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths to Refresh (1000 Max)'),
    '#rows' => 6,
    '#description' => t('One path per line. Paths can be full URL, root path URL (e.g. /node/1234) or Drupal paths (e.g. node/1234).  These will be normalized to root paths, e.g. http://example.com/node/1234 = /node/1234. Drupal paths will be checked for aliases and the alias purged as well, e.g. node/1234 will have /node/1234 and /content/my_title alias clear . The home page can be identified by <front>. Wild cards are NOT supported.'),
  );

  $form['normalized_paths'] = array(
    '#type' => 'value',
    '#value' => array(),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Refresh',
  );

  return $form;
}
/**
 * Validate the purge form.
 *
 * Normalizes the entered values and checks that under 1000 were entered.
 *
 * @param array $form
 *   The Form array.
 * @param array $form_state
 *   The Form state array.
 */
function cloudfront_refresh_selective_purge_form_validate(array $form, array &$form_state) {
  $paths = explode("\n", $form_state['values']['paths']);
  $normalize_paths = cloudfront_refresh_normalize_paths($paths);
  if (count($normalize_paths) > 1000) {
    form_set_error('paths', t("Only 1000 paths may be invalidated at a time."));
  }
  form_set_value($form['normalized_paths'], $normalize_paths, $form_state);
}

/**
 * Refresh manually entered URLs.
 *
 * @param array $form
 *   The Form array.
 * @param array $form_state
 *   The Form state array.
 */
function cloudfront_refresh_selective_purge_form_submit(array $form, array $form_state) {

  // Create a purge the paths.
  $normalized_paths = $form_state['values']['normalized_paths'];
  if (!empty($normalized_paths)) {
    cloudfront_refresh_invalidate_urls($normalized_paths);
  }
}
/**
 * Page callback to list invalidate request information.
 *
 * @return array
 *   The render array for the page.
 */
function cloudfront_refresh_status() {

  $output = array();

  $opts = array(
    'MaxItems' => CLOUDFRONT_REFRESH_MAX_STATUS_IDS,
  );
  $response = cloudfront_refresh_list_invalidations($opts);
  if ($response === NULL) {
    return array(
      '#markup' => t('No requests to invalidate content on CloudFront found.'),
    );
  }

  $info = $response->body->to_stdClass();
  $ids = array();
  if (isset($info->InvalidationSummary)) {
    foreach ($info->InvalidationSummary as $item) {
      $ids[] = $item->Id;
    }
  }
  if (empty($ids)) {
    return array(
      '#markup' => t('No requests to invalidate content on CloudFront found.'),
    );
  }

  $rows = array();
  foreach ($ids as $id) {
    $response = cloudfront_refresh_get_invalidation($id);

    $details = $response->body->to_stdClass();
    if ($details !== NULL) {
      $created = date("d M, Y", strtotime($details->CreateTime));
      $status = $details->Status;
      $batch = $details->InvalidationBatch;
      $batch_paths = array();
      if (is_array($batch->Path)) {
        $batch_paths = $batch->Path;
      }
      else {
        $batch_paths[] = $batch->Path;
      }
      $quantity = 0;
      $paths = '';
      $max_urls = CLOUDFRONT_REFRESH_MAX_STATUS_URLS;
      foreach ($batch_paths as $value) {
        if ($quantity > 0  && $quantity <= $max_urls) {
          $paths .= "<br/>\n";
        }
        if ($quantity < $max_urls) {
          $paths .= check_plain($value);
        }
        if ($quantity == $max_urls) {
          $paths .= "(more URLs not listed)";
        }
        $quantity++;
      }
      $rows[] = array(
        check_plain($id), $created, check_plain($status), $quantity, $paths,
      );
    }
  }

  $queue = DrupalQueue::get(CLOUDFRONT_REFRESH_QUEUE);
  $pending = $queue->numberOfItems();
  if ($pending > 0) {
    $queued = t("There are @pending URLs in the queue.  Clear cache to process them.", array(
      '@pending' => $pending)
    );
  }
  else {
    $queued = t("There are no URLs in the queue.");
  }

  $output['queue_status_header'] = array(
    '#type' => 'markup',
    '#markup' => "<h2>Refresh Queue Information</h2>",
  );
  $output['queue_status'] = array(
    '#type' => 'markup',
    '#markup' => "<div>{$queued}</div>",
  );

  $output['cs_status_header'] = array(
    '#type' => 'markup',
    '#markup' => "<h2>CloudFront Invalidation Status Information</h2>",
  );
  $header = array('Id', 'Created', 'Status', 'Num URLs', 'URLs');
  $output['cf_status'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $output;
}
/**
 * Retrieve a list of invalidations from CF. Defaults to the most recent 100.
 *
 * @param array $opts
 *   The AWS SDK list_invalidations() options.
 *
 * @return mixed
 *   NULL | CFResponse
 */
function cloudfront_refresh_list_invalidations(array $opts = NULL) {
  libraries_load('awssdk');

  $cloudfront = new AmazonCloudFront();

  $distribution_id = variable_get('cloudfront_refresh_distribution_id', '');
  if (empty($distribution_id)) {
    drupal_set_message(t('Cloudfront settings have not be set.'), 'warning', FALSE);
    return NULL;
  }

  $result = $cloudfront->list_invalidations($distribution_id, $opts);

  if (!$result->isOK()) {
    $http_status = check_plain($result->status);
    $error_message = check_plain(strip_tags($result->body->Error->Message->asXML()));
    drupal_set_message(t('An error occurred when invalidating CloudFront edge cache. Message: @error_message (HTTP status: @http_status)',
      array('@error_message' => $error_message, '@http_status' => $http_status)), 'error');
    return NULL;
  }
  return $result;
}
/**
 * Get information about a specific invalidation.
 *
 * @param string $id
 *   The invalidation ID to get information for.
 * @param array $opts
 *   The SDK get_invalidation() options.
 *
 * @return mixed
 *   NULL | CFResponse
 */
function cloudfront_refresh_get_invalidation($id, array $opts = NULL) {
  libraries_load('awssdk');

  $cloudfront = new AmazonCloudFront();

  $distribution_id = variable_get('cloudfront_refresh_distribution_id', '');
  if (empty($distribution_id)) {
    drupal_set_message(t('Cloudfront settings have not be set.'), 'warning', FALSE);
    return NULL;
  }

  $result = $cloudfront->get_invalidation($distribution_id, $id, $opts);

  if (!$result->isOK()) {
    $http_status = check_plain($result->status);
    $error_message = check_plain(strip_tags($result->body->Error->Message->asXML()));
    drupal_set_message(t('An error occurred when invalidating CloudFront edge cache. Message: @error_message (HTTP status: @http_status)',
      array('@error_message' => $error_message, '@http_status' => $http_status)), 'error');
    return NULL;
  }
  return $result;
}
