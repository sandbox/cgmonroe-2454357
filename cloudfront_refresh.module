<?php

/**
 * @file
 * Module to help quickly refresh files served via Amazon's CloudFront CDN.
 */

define('CLOUDFRONT_REFRESH_MAX_STATUS_URLS', 10);
define('CLOUDFRONT_REFRESH_MAX_STATUS_IDS', 20);
define('CLOUDFRONT_REFRESH_QUEUE', 'cloudfront_refresh_queue');

/**
 * Implements hook_permission().
 */
function cloudfront_refresh_permission() {
  return array(
    'administer cloudfront' => array(
      'title' => 'Administer CloudFront CDN',
      'description' => t('User can view / configure the module settings.'),
      'restrict access' => TRUE,
    ),
    'purge cloudfront' => array(
      'title' => 'Purge CloudFront',
      'description' => t('Manually specify paths to clear from the CDN cache.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function cloudfront_refresh_menu() {
  $items = array();

  $items['admin/config/development/cloudfront_refresh'] = array(
    'title' => 'CloudFront Refresh Tool',
    'description' => 'Configure your CDN integration and force clear the cache.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudfront_refresh_selective_purge_form'),
    'access arguments' => array('purge cloudfront'),
    'type' => MENU_NORMAL_ITEM,
    'weight' => -5,
    'file' => 'cloudfront_refresh.pages.inc',
    'file path' => drupal_get_path('module', 'cloudfront_refresh') . '/includes',
  );

  $items['admin/config/development/cloudfront_refresh/purge'] = array(
    'title' => 'Refresh Pages',
    'description' => 'Tell CloudFront to clear the CDN cache for specific URLS.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudfront_refresh_selective_purge_form'),
    'access arguments' => array('purge cloudfront'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -5,
    'file' => 'cloudfront_refresh.pages.inc',
    'file path' => drupal_get_path('module', 'cloudfront_refresh') . '/includes',
  );

  $items['admin/config/development/cloudfront_refresh/status'] = array(
    'title' => 'Status',
    'page callback' => 'cloudfront_refresh_status',
    'access arguments' => array('purge cloudfront'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cloudfront_refresh.pages.inc',
    'file path' => drupal_get_path('module', 'cloudfront_refresh') . '/includes',
  );

  $items['admin/config/development/cloudfront_refresh/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudfront_refresh_settings_form'),
    'access arguments' => array('administer cloudfront'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'cloudfront_refresh.pages.inc',
    'file path' => drupal_get_path('module', 'cloudfront_refresh') . '/includes',
  );

  return $items;
}

/**
 * Take an array of paths and normalize them to '/xxxx/yyy' form used by AWS.
 *
 * Converts full urls with domains.  Note it does not check if domain matches.
 * Converts <front> and Drupal paths and adds in aliases if found.
 * Removes any attempted wildcard entries.
 * Removes blank lines.
 *
 * @param array $paths
 *   An array who's values need to be normalized.
 *
 * @return array
 *    An array of paths with values as the normalized path or an empty array.
 */
function cloudfront_refresh_normalize_paths(array $paths) {
  $normalized_paths = array();
  foreach ($paths as $path) {
    $path = trim($path);

    if (empty($path)) {
      continue;
    }
    // Throw out any wildcard attempts.
    if (strpos($path, '*') !== FALSE) {
      continue;
    }

    // Trim any schema / host info.
    if (strpos($path, "http") == 0) {
      $path = parse_url($path, PHP_URL_PATH);
    }
    // Drupal path format.
    if (strpos($path, "/") !== 0) {
      if (drupal_strtolower($path) == '<front>') {
        $path = url($path);
      }
      // See if there is an alias.
      else {
        $p = base_path() . $path;
        $alias = url($path);
        if ($alias != $p) {
          $path = $p;
          $normalized_paths[] = $alias;
        }
      }
      // Test if path IS an alias.
      $src_path = drupal_lookup_path("source", $path);
      if ($src_path !== FALSE) {
        $normalized_paths[] = base_path() . $src_path;
        $path = base_path() . $path;
      }
    }
    $normalized_paths[] = $path;
  }
  return $normalized_paths;
}

/**
 * Implements hook_help().
 */
function cloudfront_refresh_help($path, $arg) {
  switch ($path) {
    // Help for the main module configuration page.
    case 'admin/config/development/cloudfront_refresh/settings':
      return '<p>' . t("Enter the CloudFront Distribution ID used as this site's CDN below. This value will be an alphanumeric key, not the base URL of the CDN. This module depends on AWS SDK and expects that keys for AWS have been set separately from this form (see AWS SDK UI module).") . '</p>';
  }
}

/**
 * Queue invalidate requests to be grouped into a single invalidate request.
 *
 * @param array $paths
 *   An array of paths to add to queue.
 * @param bool $quiet
 *   If TRUE don't display queued message to the user.
 */
function cloudfront_refresh_queue_invalidations(array $paths, $quiet = FALSE) {
  $queue = DrupalQueue::get(CLOUDFRONT_REFRESH_QUEUE);

  foreach ($paths as $path) {
    $queue->createItem(array('path' => $path));
  }
  drupal_set_message(t('Item added to CloudFront refresh queue. Content will be refreshed when cache is next cleared.'));
}
/**
 * Bulk process all the currently queued items.
 */
function cloudfront_refresh_process_queue() {

  if (!cloudfront_refresh_is_configured()) {
    return;
  }
  $queue = DrupalQueue::get(CLOUDFRONT_REFRESH_QUEUE);

  $paths = array();
  while ($item = $queue->claimItem()) {
    $paths[] = $item->data['path'];
    $queue->deleteItem($item);
  }
  if (empty($paths)) {
    return;
  }
  cloudfront_refresh_bulk_invalidate($paths);
}
/**
 * Invalidate one or more paths from a CloudFront distribution.
 *
 * @param array $paths
 *    An indexed array of paths to be cleared. These paths must be relative to
 *    the CloudFront distribution root. For example 'foo.jpg', not
 *   'http://cdn.example.com/foo.jpg'.
 *
 * @return bool
 *    TRUE or FALSE success response from CloudFront.
 *
 * @todo Connections to CloudFront should be tested.
 */
function cloudfront_refresh_invalidate_urls(array $paths) {
  libraries_load('awssdk');

  $cloud_front = new AmazonCloudFront();

  $distribution_id = variable_get('cloudfront_refresh_distribution_id', '');
  if (empty($distribution_id)) {
    drupal_set_message(t('Cloudfront settings have not be set.'), 'warning', FALSE);
    return FALSE;
  }
  $caller_reference = 'Drupal CloudFront Refresh ' . date('Y-m-d H:i:s');

  $invalidation = $cloud_front->create_invalidation($distribution_id, $caller_reference, $paths);

  if ($invalidation->isOK()) {
    drupal_set_message(t('CloudFront cache invalidation in progress. Invalidation is normally complete in 5 to 20 minutes but can occasionally take longer.'));
  }
  else {
    $http_status = check_plain($invalidation->status);
    $error_message = check_plain(strip_tags($invalidation->body->Error->Message->asXML()));
    drupal_set_message(t('An error occurred when invalidating CloudFront edge cache. Message: @error_message (HTTP status: @http_status)',
      array('@error_message' => $error_message, '@http_status' => $http_status)), 'error');
  }
  return $invalidation->isOK();
}
/**
 * Implements hook_expire_cache().
 *
 * React to Expire v2.x module cache clearing
 *
 * @see expire.api.inc
 */
function cloudfront_refresh_expire_cache($urls, $wildcards, $object_type, $object) {
  cloudfront_refresh_bulk_invalidate($urls);
}
/**
 * Create a set of invalidation requests for multiple paths.
 *
 * Will normalize and remove any duplicate paths.
 *
 * @param array $paths
 *   The paths to be sent in the invalidate request.
 */
function cloudfront_refresh_bulk_invalidate(array $paths) {

  $paths = array_merge($paths, cloudfront_refresh_always_purge_paths());
  $paths = cloudfront_refresh_normalize_paths($paths);
  $paths = array_unique($paths);

  if (count($paths) > 1000) {
    foreach (array_chunk($paths, 1000) as $chunk) {
      cloudfront_refresh_invalidate_urls($chunk);
    }
  }
  else {
    cloudfront_refresh_invalidate_urls($paths);
  }
}

// Disable hook_node_* code if the expire.module is enabled.
if (!module_exists('expire')) {
  module_load_include('inc', 'cloudfront_refresh', 'includes/cloudfront_refresh.node');
}
/**
 * Allow a set of URLs to be defined as alway purge when something changes.
 *
 * @return array
 *   Array of normalized paths or empty array
 */
function cloudfront_refresh_always_purge_paths() {
  $paths = array();
  $always_purge = variable_get('cloudfront_refresh_purge_always', '');
  if ($always_purge) {
    $paths = explode("\n", $always_purge);
    $paths = cloudfront_refresh_normalize_paths($paths);
  }
  return $paths;
}
/**
 * Implements hook_process_html().
 *
 * Make the css and js files in the header non-domain specific.
 */
function cloudfront_refresh_process_html(&$variables) {
  global $base_root;

  $css = str_replace($base_root, '', $variables['styles']);
  $variables['styles'] = $css;

  $js = str_replace($base_root, '', $variables['scripts']);
  $variables['scripts'] = $js;
}
/**
 * Implements hook_advagg_context_alter().
 */
function cloudfront_refresh_advagg_context_alter(&$original, $aggregate_settings, $mode) {
  $cdn_host = trim(variable_get('cloudfront_refresh_host_name', ""));
  if (!$cdn_host) {
    return;
  }
  // Change context.
  if ($mode == 0) {

    // Save original settings.
    $original['base_root'] = $GLOBALS['base_root'];
    $original['base_url'] = $GLOBALS['base_url'];
    $original['base_path'] = $GLOBALS['base_path'];
    $original['is_https'] = $GLOBALS['is_https'];
    $original['language'] = isset($GLOBALS['language']) ? $GLOBALS['language'] : NULL;

    $GLOBALS['is_https'] = $aggregate_settings['variables']['is_https'];
    if ($aggregate_settings['variables']['is_https']) {
      $GLOBALS['base_root'] = str_replace('http://', 'https://', $GLOBALS['base_root']);
      $GLOBALS['base_url'] = str_replace('http://', 'https://', $GLOBALS['base_url']);
    }
    else {
      $GLOBALS['base_root'] = str_replace('https://', 'http://', $GLOBALS['base_root']);
      $GLOBALS['base_url'] = str_replace('https://', 'http://', $GLOBALS['base_url']);
    }
    $GLOBALS['base_path'] = $aggregate_settings['variables']['base_path'];

    if (isset($aggregate_settings['variables']['language'])) {
      $languages = language_list();
      if (isset($languages[$aggregate_settings['variables']['language']])) {
        $GLOBALS['language'] = $languages[$aggregate_settings['variables']['language']];
      }
    }
    // Force CDN host to be used for aggregation.
    if ($cdn_host && strcasecmp($_SERVER['HTTP_HOST'], $cdn_host) != 0) {
      $GLOBALS['base_root'] = preg_replace("/{$_SERVER['HTTP_HOST']}/i", $cdn_host, $GLOBALS['base_root']);
      $GLOBALS['base_url'] = preg_replace("/{$_SERVER[HTTP_HOST]}/i", $cdn_host, $GLOBALS['base_url']);
    }
  }
  // Change context back.
  elseif ($mode == 1) {
    if (isset($original['base_root'])) {
      $GLOBALS['base_root'] = $original['base_root'];
    }
    if (isset($original['base_url'])) {
      $GLOBALS['base_url'] = $original['base_url'];
    }
    if (isset($original['base_path'])) {
      $GLOBALS['base_path'] = $original['base_path'];
    }
    if (isset($original['is_https'])) {
      $GLOBALS['is_https'] = $original['is_https'];
    }
    if (isset($original['language'])) {
      $GLOBALS['language'] = $original['language'];
    }
  }
}
/**
 * Implements hook_flush_caches().
 */
function cloudfront_refresh_flush_caches() {
  return array('cloudfront_refresh');
}
/**
 * Check if all the required settings have be configured in the admin screens.
 *
 * @return bool
 *   True if configured / False if not
 */
function cloudfront_refresh_is_configured() {
  return variable_get('aws_key', '') && variable_get('aws_secret', '') && variable_get('cloudfront_refresh_distribution_id', '');
}
